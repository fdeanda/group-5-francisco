var toolInputValue, setTypeValue, materialValue;

function setSpecificToolForm() {
    var e = document.getElementById("set-type");
    setTypeValue = e.options[e.selectedIndex].value;
    // console.log(setTypeValue);

    if(setTypeValue == "armor"){
        document.getElementById("specific_tool_input").innerHTML = "<select id = \"specific-tool\" onchange=setMaterialForm()>\n" +
            "            <option value=\"\">Armor Piece</option>\n" +
            "            <option value=\"helmet\">Helmet</option>\n" +
            "            <option value=\"chestplate\">Chestplate</option>\n" +
            "            <option value=\"leggings\">Leggings</option>\n" +
            "            <option value=\"boots\">Boots</option>\n" +
            "        </select>"
    } else if(setTypeValue == "tools"){
        document.getElementById("specific_tool_input").innerHTML = "<select id = \"specific-tool\" onchange=setMaterialForm()>\n" +
            "            <option value=\"\">Tool Type</option>\n" +
            "            <option value=\"sword\">Sword</option>\n" +
            "            <option value=\"pickaxe\">Pickaxe</option>\n" +
            "            <option value=\"shovel\">Shovel</option>\n" +
            "            <option value=\"axe\">Axe</option>\n" +
            "            <option value=\"hoe\">Hoe</option>\n" +
            "        </select>"
    }
}

function setMaterialForm(){
    var e = document.getElementById("specific-tool");
    toolInputValue = e.options[e.selectedIndex].value;
    //console.log(toolInputValue);

    if(toolInputValue == "helmet" || toolInputValue == "chestplate" || toolInputValue == "leggings" || toolInputValue == "boots"){
        document.getElementById("material_input").innerHTML = "<select id = \"material\" onchange=setRecipe()>\n" +
            "            <option value=\"\">Armor Material</option>\n" +
            "            <option value=\"leather\">Leather</option>\n" +
            "            <option value=\"iron\">Iron</option>\n" +
            "            <option value=\"gold\">Gold</option>\n" +
            "            <option value=\"diamond\">Diamond</option>\n" +
            "        </select>"
    } else if(toolInputValue == "sword" || toolInputValue == "pickaxe" || toolInputValue == "shovel" || toolInputValue == "axe" || toolInputValue == "hoe"){
        document.getElementById("material_input").innerHTML = "<select id = \"material\" onchange=setRecipe()>\n" +
            "            <option value=\"\">Tool Material</option>\n" +
            "            <option value=\"wood\">Wood</option>\n" +
            "            <option value=\"iron\">Iron</option>\n" +
            "            <option value=\"gold\">Gold</option>\n" +
            "            <option value=\"diamond\">Diamond</option>\n" +
            "        </select>"
    }
}

function setRecipe(){
    var e = document.getElementById("material");
    materialValue = e.options[e.selectedIndex].value;
    //console.log(setTypeValue, toolInputValue, materialValue);

    /*
    if(setype == armor, tool == pickaxe, material == diamond){ show image for diamond pickaxe recipe}
     */

    if(setTypeValue == "armor"){
        if(toolInputValue == "helmet"){
            if(materialValue == "leather"){
                console.log("leather helmet");
                document.getElementById("recipe_image").innerHTML = "<img src=images/leatherhelm.png>"
            } else if(materialValue == "iron"){
                console.log("iron helmet");
                document.getElementById("recipe_image").innerHTML = "<img src='images/ironhelm.png'>"
            } else if(materialValue == "gold"){
                console.log("gold helmet");
                document.getElementById("recipe_image").innerHTML = "<img src=images/goldhelm.png>"
            } else if(materialValue == "diamond"){
                console.log("diamond helmet");
                document.getElementById("recipe_image").innerHTML = "<img src=images/diamondhelm.png>"
            }
        } else if(toolInputValue == "chestplate"){
            if(materialValue == "leather"){
                console.log("leather chestplate");
                document.getElementById("recipe_image").innerHTML = "<img src=images/leatherchest.png>"
            } else if(materialValue == "iron"){
                console.log("iron chestplate");
                document.getElementById("recipe_image").innerHTML = "<img src=images/ironchest.png>"
            } else if(materialValue == "gold"){
                console.log("gold chestplate");
                document.getElementById("recipe_image").innerHTML = "<img src=images/goldchest.png>"
            } else if(materialValue == "diamond"){
                console.log("diamond chestplate");
                document.getElementById("recipe_image").innerHTML = "<img src=images/diamondchest.png>"
            }
        } else if(toolInputValue == "leggings"){
            if(materialValue == "leather"){
                console.log("leather pants");
                document.getElementById("recipe_image").innerHTML = "<img src=images/leatherlegs.png>"
            } else if(materialValue == "iron"){
                console.log("iron pants");
                document.getElementById("recipe_image").innerHTML = "<img src=images/ironlegs.png>"
            } else if(materialValue == "gold"){
                console.log("gold pands");
                document.getElementById("recipe_image").innerHTML = "<img src=images/goldlegs.png>"
            } else if(materialValue == "diamond"){
                console.log("diamond pants");
                document.getElementById("recipe_image").innerHTML = "<img src=images/diamondpants.png>"
            }
        } else if(toolInputValue == "boots"){
            if(materialValue == "leather"){
                console.log("leather boots");
                document.getElementById("recipe_image").innerHTML = "<img src=images/leatherboots.png>"
            } else if(materialValue == "iron"){
                console.log("iron boots");
                document.getElementById("recipe_image").innerHTML = "<img src=images/ironboots.png>"
            } else if(materialValue == "gold"){
                console.log("gold boots");
                document.getElementById("recipe_image").innerHTML = "<img src=images/goldboots.png>"
            } else if(materialValue == "diamond"){
                console.log("diamond boots");
                document.getElementById("recipe_image").innerHTML = "<img src=images/diamondboots.png>"
            }
        }
    } else if(setTypeValue == "tools"){
        if(toolInputValue == "sword"){
            if(materialValue == "wood"){
                console.log("wooden sword");
                document.getElementById("recipe_image").innerHTML = "<img src=images/woodsword.png>"
            } else if(materialValue == "iron"){
                console.log("iron sword");
                document.getElementById("recipe_image").innerHTML = "<img src=images/ironsword.png>"
            } else if(materialValue == "gold"){
                console.log("gold sword");
                document.getElementById("recipe_image").innerHTML = "<img src=images/goldsword.png>"
            } else if(materialValue == "diamond"){
                console.log("diamond sword");
                document.getElementById("recipe_image").innerHTML = "<img src=images/diamondsword.png>"
            }
        } else if(toolInputValue == "pickaxe"){
            if(materialValue == "wood"){
                console.log("wooden pickaxe");
                document.getElementById("recipe_image").innerHTML = "<img src=images/woodpick.png>"
            } else if(materialValue == "iron"){
                console.log("iron pickaxe");
                document.getElementById("recipe_image").innerHTML = "<img src=images/ironpick.png>"
            } else if(materialValue == "gold"){
                console.log("gold pickaxe");
                document.getElementById("recipe_image").innerHTML = "<img src=images/goldpick.png>"
            } else if(materialValue == "diamond"){
                console.log("diamond pickaxe");
                document.getElementById("recipe_image").innerHTML = "<img src=images/diamondpick.png>"
            }
        } else if(toolInputValue == "shovel"){
            if(materialValue == "wood"){
                console.log("wooden shovel");
                document.getElementById("recipe_image").innerHTML = "<img src=images/woodshovel.png>"
            } else if(materialValue == "iron"){
                console.log("iron shovel");
                document.getElementById("recipe_image").innerHTML = "<img src=images/ironshovel.png>"
            } else if(materialValue == "gold"){
                console.log("gold shovel");
                document.getElementById("recipe_image").innerHTML = "<img src=images/goldshovel.png>"
            } else if(materialValue == "diamond"){
                console.log("diamond shovel");
                document.getElementById("recipe_image").innerHTML = "<img src=images/diamondshovel.png>"
            }
        } else if(toolInputValue == "axe"){
            if(materialValue == "wood"){
                console.log("wooden axe");
                document.getElementById("recipe_image").innerHTML = "<img src=images/woodaxe.png>"
            } else if(materialValue == "iron"){
                console.log("iron axe");
                document.getElementById("recipe_image").innerHTML = "<img src=images/ironaxe.png>"
            } else if(materialValue == "gold"){
                console.log("gold axe");
                document.getElementById("recipe_image").innerHTML = "<img src=images/goldaxe.png>"
            } else if(materialValue == "diamond"){
                console.log("diamond axe");
                document.getElementById("recipe_image").innerHTML = "<img src=images/diamondaxe.png>"
            }
        } else if(toolInputValue == "hoe"){
            if(materialValue == "wood"){
                console.log("wooden hoe");
                document.getElementById("recipe_image").innerHTML = "<img src=images/woodhoe.png>"
            } else if(materialValue == "iron"){
                console.log("iron hoe");
                document.getElementById("recipe_image").innerHTML = "<img src=images/ironhoe.png>"
            } else if(materialValue == "gold"){
                console.log("gold hoe");
                document.getElementById("recipe_image").innerHTML = "<img src=images/goldhoe.png>"
            } else if(materialValue == "diamond"){
                console.log("diamond hoe");
                document.getElementById("recipe_image").innerHTML = "<img src=images/diamondhoe.png>"
            }
        }
    }
}